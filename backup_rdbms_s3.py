#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Backup non-system RDBMS bases to a S3 bucket
"""

import os
import socket
import tempfile
import logging
import argparse
import shutil
from datetime import date

import boto3
from botocore.exceptions import ClientError
import psycopg2
import mariadb


class Backup:
    """
    Root object to persist the bucket parameters
    """

    def __init__(self, bucket_name):
        """Constructor"""
        self.hostname = os.getenv("HOSTNAME", socket.gethostname())
        logging.info("Using hostname %s", self.hostname)
        endpoint = os.getenv("S3_ENDPOINT")
        access_key = os.getenv("S3_ACCESS_KEY_ID")
        secret_key = os.getenv("S3_SECRET_ACCESS_KEY")
        logging.info("Using endpoint %s", endpoint)
        self.s3client = boto3.client(
            service_name="s3",
            endpoint_url=endpoint,
            aws_access_key_id=access_key,
            aws_secret_access_key=secret_key,
        )
        self.bucket = bucket_name
        self.now = date.today().isoformat()

    @staticmethod
    def list_mariadb_bases():
        """List all MariaDB databases except the system ones"""
        con = mariadb.connect(user="root", unix_socket="/run/mysqld/mysqld.sock")
        cur = con.cursor()
        cur.execute("show databases")
        return [
            row[0]
            for row in cur.fetchall()
            if row[0]
            not in ("mysql", "information_schema", "phpmyadmin", "performance_schema")
        ]

    @staticmethod
    def list_pgsql_bases():
        """List all PostgreSQL databases except the system ones"""
        dbargs = {
            "dbname": os.getenv("PGDATABASE", "postgres"),
            "user": os.getenv("PGUSER", "postgres"),
            "password": os.getenv("PGPASSWORD"),
            "port": os.getenv("PGPORT", 5432),
        }
        if "PGHOST" in os.environ:
            dbargs["host"] = os.environ["PGHOST"]
        with psycopg2.connect(**dbargs) as conn:
            with conn.cursor() as cur:
                cur.execute(
                    "SELECT datname FROM pg_database WHERE datistemplate = false;"
                )
                return [
                    row[0]
                    for row in cur.fetchall()
                    if row[0] not in ("postgres", "template0", "template1")
                ]

    @staticmethod
    def dump_mariadb_base(base, dumpfile):
        """Dump a MariaDB base to the filesystem"""
        os.system(
            f"mysqldump --defaults-file=/etc/mysql/debian.cnf --result-file={dumpfile} {base}"
        )

    @staticmethod
    def dump_pgsql_base(base, dumpfile):
        """Dump a PostgreSQL base to the filesystem"""
        os.system(f"pg_dump -E UTF-8 -f {dumpfile} {base}")

    def upload_backup(self, dumpfile, base, rdbms_type):
        """Upload a DB dump to S3"""
        object_name = f"{self.hostname}/{rdbms_type}/{self.now}/{base}.sql.xz"
        try:
            logging.debug("local file: %s", dumpfile)
            logging.debug("object name: %s", object_name)
            self.s3client.upload_file(dumpfile, self.bucket, object_name)
        except ClientError as err:
            logging.error(err)

    def backup_bases(self, rdbms_type="mariadb"):
        """Main backup logic"""
        bases = getattr(self, f"list_{rdbms_type}_bases")()
        with tempfile.TemporaryDirectory() as directory:
            for base in bases:
                logging.debug("Dumping base %s", base)
                dumpfile = os.path.join(directory, f"{base}.sql")
                getattr(self, f"dump_{rdbms_type}_base")(base, dumpfile)
                os.system(f"xz {dumpfile}")
                logging.debug("Uploading base %s", base)
                self.upload_backup(dumpfile + ".xz", base, rdbms_type)
            shutil.rmtree(directory)


def main():
    """Main method"""
    parser = argparse.ArgumentParser(description="Backup RDBMS bases to S3")
    parser.add_argument("bucket", help="S3 bucket to use")
    parser.add_argument("-d", "--debug", action="store_true")
    parser.add_argument("-v", "--verbose", action="store_true")
    parser.add_argument(
        "-t",
        "--type",
        help="Type of bases to backup",
        default="mariadb",
        choices=["mariadb", "pgsql"],
    )
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    elif args.verbose:
        logging.basicConfig(level=logging.INFO)

    backup = Backup(args.bucket)
    backup.backup_bases(rdbms_type=args.type)


if __name__ == "__main__":
    main()
