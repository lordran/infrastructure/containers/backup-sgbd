FROM docker.io/postgres:14

RUN apt-get update && \
    apt-get -y install python3 python3-psycopg2 python3-pip python3-boto3 libmariadb-dev && \
    pip3 install -U mariadb

COPY backup_rdbms_s3.py /

ENTRYPOINT ["/backup_rdbms_s3.py"]
